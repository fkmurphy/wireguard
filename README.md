## Mi configuración de Wireguard
Se utiliza la imagen de linuxsever/wireguard y se añade un ejemplo de cómo cargo las reglas de iptables al iniciar el servicio.

Ver m�s en scripts/postup.sh.

Falta archivo de configuraci�n, este se genera luego del primer inicio: ```config/wg0.conf```
Reemplazar la linea PostUp completa por:
```bash
PostUp = /scripts/postup.sh
```
