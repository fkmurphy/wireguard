#!/bin/sh
iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 5880 -j DNAT --to-destination 10.33.3.4:80
iptables -t nat -A POSTROUTING -d 10.33.3.4 -p tcp --dport 80 -j MASQUERADE
